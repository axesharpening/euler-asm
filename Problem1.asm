section .data                           ;Data segment
    probTitle db 'Problem 1: Multiples of 3 and 5',10,0 ;join string to 10=CR
    lenprobTitle equ $-probTitle             ;The length of the message
    probDesc db 'If we list all the natural numbers below 10 that are multiples of 3 or 5, we get 3, 5, 6 and 9. The sum of these multiples is 23. Find the sum of all the multiples of 3 or 5 below 1000.',10,0 
    lenprobDesc equ $-probDesc             ;The length of the message
    dispMsg db 'Result: ' , 10,0
    dispWorkMsg db 'Working: ', 10,0

section	.bss   ;declaring variables Uninitialized Data
    numI resb 1  ;RESB=Reserve a Byte 1 , RESW=Reserve a Word , RESD=Reserve a Doubleword
    numE resb 16  ;RESB=Reserve a Byte 1 , RESW=Reserve a Word , RESD=Reserve a Doubleword

section .text          ;Code Segment
   global _start

_start:	         ;tell linker entry point
   ;Output the Problem title
   mov eax, 4                   ;file descriptor (stdout)
   mov ebx, 1                   ;system call number (sys_write)
   mov ecx, probTitle           ;Set the text
   mov edx, lenprobTitle        ;Set the Length
   int 80h                      ;call kernel to display on screen
 
   ;Output the Problem Description
   mov eax, 4                   ;file descriptor (stdout)
   mov ebx, 1                   ;system call number (sys_write)
   mov ecx, probDesc            ;Set the text
   mov edx, lenprobDesc         ;Set the Length
   int 80h                      ;call kernel to display on screen
 

    ;PROBLEM Begin 
 
    mov ecx , 0              ;start iterator
    mov eax , 1000           ;Max iterator
    
l1:
    push eax ;Move 1000 to the stack ready for later 

    inc ecx                  ;incrementing by +1
    mov [numI], ecx 

    mov ecx, numI         ;Set the text       
    call _writeLine 

    xor ecx,ecx
    mov ecx, [numI] 

    pop eax ;Move 1000 back from the stack ready for testing
    cmp eax, ecx
jle l1 ; Jump Less/Equal or Jump Not Greater



; ;;====First try====================
;     mov ecx, 1000               ;Max loop iterations
;     mov eax, 1                  ;Start
	
; l1:
;     inc eax  ; Increments the count variable
;     mov [numI], eax
; ;    mov eax, 4
; ;    mov ebx, 1
;     push ecx
       
;     ;First check if 3 is devisable
;     mov	    ax, byte[numI]
;     sub     ax, '0'  ;subtracting ascii '0' to convert it into a decimal number
;     mov 	bx, '3'  ;Store the Number 3 to devide by
;     sub     bx, '0'  ;subtracting ascii '0' to convert it into a decimal number
;     div 	bx
;     sub	    ax, '0'  ;subtracting ascii '0' to convert it into a decimal number
;     cmp   ax, [0]    ;MOD check for a remander
;     je _addNo        ;If here is no remander the add it to the total

;     ;Second check if 5 is devisable
;     mov	    ax, byte [numI]
;     sub     ax, '0'  ;subtracting ascii '0' to convert it into a decimal number
;     mov 	bx, '5'  ;Store the Number 5 to devide by
;     sub     bx, '0'  ;subtracting ascii '0' to convert it into a decimal number
;     div 	bx
;     sub	    ax, '0'  ;subtracting ascii '0' to convert it into a decimal number
;     cmp   ax, [0]    ;MOD check for a remander
;     je _addNo        ;If here is no remander the add it to the total
 
;     ;Neither are so jump over
;     call _next

; _addNo:
;     ;Display working out message 
;     mov ecx, dispWorkMsg         
;     call _writeLine  
;     mov ecx, numI         
;     call _writeLine  

;     mov eax, [numE]
;     sub eax, '0'
	
;     mov ebx, [numI]
;     sub ebx, '0'
    
;     add eax, ebx        ; add eax and ebx
   
;     add eax, '0'        ; add '0' to to convert the sum from decimal to ASCII
;     mov [numE], eax     ; store total in variable 

; _next:
;    int 0x80
	
;    mov eax, [numI]
;    sub eax, '0'
;    inc eax
;    add eax, '0'
;    pop ecx
;    loop l1
; ;;========================


    ; Display result
    mov ecx, dispMsg       ;Set the textt       
    call _writeLine  

    mov ecx, numE        ;Set the textt       
    call _writeLine 

    ;PROBLEM End
 
 _exit:
   ;=== Exit code ====
   mov eax, 1    ;system call number (sys_exit)
   mov ebx, 0    ;Success
   int 80h       ;call kernel
   ret


;;===Start of Functions====

;This is my own little function to write variable length strings out to screan without knowing the length
;This function assumes you have set ECX with the pointer to the string before calling this function  
_writeLine:  
    push ebx                    ; push a value to the stack save caller's ebx - they're probably using it

    mov eax, 4                   ;file descriptor (stdout)
    mov ebx, 1                   ;system call number (sys_write)

;get length in edx by looping round countng the Char intill string terminator
    xor edx, edx ;Set edx=0
getlen:
    cmp byte [ecx + edx], 0 ;compare the char to null byte at the end of the string
    jz gotlen               ;Jump out        
    inc edx                 ;Increase edx by one
    jmp getlen              ;get next char
gotlen: 

    int 80h                      ;call kernel to display on screen

    pop ebx                     ; restoring whatever is on top of the stack into a register

    ret  ;End return




; read:
;    cmp byte[ecx], NUL        ; NUL indicates the end of the string
;    je exit                 ; if reached the NUL terminator, exit

;    ; setup the registers for a sys_write call
;    mov eax, SYS_WRITE      ; syscall number for sys_write
;    mov ebx, STDOUT         ; print to stdout
;    mov edx, 1              ; write 1 char at a time
;    int TRAP;               ; execute the syscall

;    inc ecx                 ; increment the pointer to the next char
;    jmp read                ; loop back to read







; get length in edx
;     xor edx, edx
; getlen:
;     cmp byte [ecx + edx], 0
;     jz gotlen
;     inc edx
;     jmp getlen
; gotlen:


 
; _strlen:

;   push ebx
;   push ecx

;   mov   ebx, edi            
;   xor   al, al                               
;   mov   ecx, 0xffffffff     
                                                     
;   repne scasb               ; REPeat while Not Equal [edi] != al

;   sub   edi, ebx            ; length = offset of (edi - ebx)
;   mov   eax, edi            

;   pop ebx
;   pop ecx
;   ret 
