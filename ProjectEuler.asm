;;============START========================

 

section .data

 

msg db 10,"Enter your string::"

 

msglen:equ $-msg

 

msg1 db 10,"***** MENU *****",0ah

 

      db "1.Enter String",0ah

 

      db "2.Calculate the length of the string",0ah

 

      db "3.Copy the string",0ah

 

      db "4.Reverse the string",0ah

 

      db "5.Palindrome",0ah

 

      db "6.Exit",0ah

 

      db "Enter your choice::"

 

msg1len:equ $-msg1

 

msg2 db "LENGTH OF THE STRING::"

 

msg2len:equ $-msg2

 

msg3 db 0ah,"COPIED STRING::",0ah

 

msg3len:equ $-msg3

 

msg4 db "REVERSE OF THE STRING::"

 

msg4len:equ $-msg4

 

msg5 db "::STRING IS PALINDROME::",0ah

 

msg5len:equ $-msg5

 

msg6 db "::STRING IS NOT PALINDROME::",0ah

 

msg6len:equ $-msg6

 

msg7 db "ORIGINAL SECOND STRING::",0ah

 

msg7len:equ $-msg7

 

section .bss

 

str1 resb 50

 

str1len:equ $-str1

 

str2 resb 50

 

str2len:equ $-str2

 

choice resb 2

 

dispnum resb 16

 

len resd 1

 

%macro cmmn 4

 

   mov rax,%1

 

   mov rdi,%2

 

   mov rsi,%3

 

   mov rdx,%4

 

   syscall

 

%endm

 

section .text

 

global _start

 

   _start:

 

       

 

    menu:

 

        cmmn 1,1,msg1,msg1len

 

        cmmn 0,0,choice,2

 

        cmp byte[choice],'1'

 

        je opt0

 

        cmp byte[choice],'2'

 

        je opt1

 

        cmp byte[choice],'3'

 

        je opt2

 

        cmp byte[choice],'4'

 

        je opt3

 

        cmp byte[choice],'5'

 

        je opt4

 

        call ext

 

    opt0:   cmmn 1,1,msg,msglen

 

        cmmn 0,0,str1,50

 

        dec eax

 

        mov [len],rax

 

        jmp menu

 

    opt1:

 

        call leng

 

        jmp menu

 

    opt2:

 

        call cpy

 

        jmp menu

 

    opt3:

 

        call rev

 

        jmp menu

 

    opt4:

 

        call palin

 

        jmp menu

 

;-------------------------------------------

 

    leng:       

 

        mov rsi,dispnum+15

 

        mov rax,[len]

 

        mov rcx,16

 

    cnt:

 

        mov rdx,0000

 

        mov rbx,10

 

        div rbx

 

        add dl,30h

 

        mov [rsi],dl

 

        dec rsi

 

        loop cnt

 

        cmmn 1,1,msg2,msg2len

 

        cmmn 1,1,dispnum,16

 

        ret 

 

;--------------------------------------------

 

    cpy:

 

        cmmn 1,1,msg7,msg7len

 

        cmmn 1,1,str2,str2len

 

        mov rcx,0000

 

        mov rcx,[len]

 

        mov rsi,str1

 

        mov rdi,str2

 

    up:

 

        mov al,[rsi]

 

        mov [rdi],al

 

        inc rsi

 

        inc rdi

 

        loop up

 

        cmmn 1,1,msg3,msg3len

 

        cmmn 1,1,str2,str1len

 

        ret

 

;--------------------------------------------

 

    rev:

 

        mov rcx,0000

 

        mov rcx,[len]

 

        mov rsi,str1

 

        add rsi,rcx

 

        sub rsi,1

 

        mov rdi,str2

 

    up1:

 

       mov al,[rsi]

 

        mov [rdi],al

 

        dec rsi

 

        inc rdi

 

        loop up1

 

        cmmn 1,1,msg4,msg4len

 

        cmmn 1,1,str2,str1len

 

        ret

 

;--------------------------------------------

 

    palin:

 

        mov rcx,0000

 

        mov rcx,[len]

 

        mov rsi,str1

 

        add rsi,rcx

 

        sub rsi,1

 

        mov rdi,str2

 

    up2:

 

        mov al,[rsi]

 

        mov [rdi],al

 

        dec rsi

 

        inc rdi

 

        loop up2

 

        mov rsi,str1

 

        mov rdi,str2

 

    loop1:

 

        mov al,[rsi]

 

        cmp al,[rdi]

 

        jne down

 

        inc rsi

 

        inc rdi

 

        dec byte[len]

 

        jnz loop1

 

        cmmn 1,1,msg5,msg5len

 

        ret

 

    down:

 

        cmmn 1,1,msg6,msg6len

 

        ret

 

;---------------------------------------------

 

    ext:

 

        mov rax,60

 

        mov rdi,00

 

        syscall

 

        ;;============END========================