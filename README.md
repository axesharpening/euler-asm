# Euler NASM

This will be the Assembly NASM Version of the Euler Problems.

https://projecteuler.net/archives


# ASM Learning Examples

Place to learn by example

http://mikeos.sourceforge.net/write-your-own-os.html

https://www.tutorialspoint.com/assembly_programming/assembly_system_calls.htm
 
Compile Basics
https://www.tutorialspoint.com/assembly_programming/assembly_basic_syntax.htm

##Compiling and Linking an Assembly Program in NASM on Linux

Install nasm on ubuntu:
$ sudo apt-get update
$ sudo apt-get install nasm 

Make sure you have set the path of nasm and ld binaries in your PATH environment variable. If you installed with apt-get then it should happen.
Now, take the following steps for compiling and linking the above program −

1. Make sure that you are in the same directory as where you saved hello.asm.
2. To assemble the program, type "nasm -f elf hello.asm" 
3. If there is any error, you will be prompted about that at this stage. Otherwise, an object file of your program named hello.o will be created. 
4. To link the object file and create an executable file named hello, type "ld -m elf_i386 -s -o hello hello.o"  
5. Execute the program by typing "./hello" 
 
##Compiling and Linking an Assembly Program in NASM on Linux

Install nasm on Windows:

1. Download and install the latest stable build. https://www.nasm.us/pub/nasm/releasebuilds/
2. Download GoLinker and Unzip http://godevtool.com/  
3. You will need to add both locations to the PATH mannualy

Make sure you have set the path of nasm and GoLink binaries in your PATH environment variable. 
Now, take the following steps for compiling and linking the above program −

1. Make sure that you are in the same directory as where you saved hello.asm.
2. To assemble the program, type "nasm -f win32 hello.asm -o hello.obj" 
3. If there is any error, you will be prompted about that at this stage. Otherwise, an object file of your program named hello.obj will be created. 
4. To link the object file and create an executable file named hello, type "GoLink.exe /console /entry _start hello.obj"  
5. Execute the program by typing "hello.exe" 

Another example:
You may have to specify your code entry point (label) with something like:

"GoLink.exe  /console /entry _start ass.obj kernel32.dll user32.dll gdi32.dll" 

Where _start could be the label where you expect your program to start. You don't need any of the DLL's listed if you aren't calling any of the Win32 API.
If you aren't creating a console application then you can leave off /console


## Some Example Solutions if you get Stuck
https://www.nayuki.io/page/project-euler-solutions

 